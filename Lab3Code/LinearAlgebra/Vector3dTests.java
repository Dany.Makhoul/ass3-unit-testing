package LinearAlgebra;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;


public class Vector3dTests {
    @Test
    public void testGetMethods(){
        Vector3d vectorTest = new Vector3d(4.6,4.2,6.7);
        assertEquals(4.6, vectorTest.getX());
        assertEquals(4.2, vectorTest.getY());
        assertEquals(6.7, vectorTest.getZ());
    }
    @Test
    public void testMagnitude(){
        Vector3d vectorTest = new Vector3d(2,2,2);
        assertEquals(3.4, vectorTest.magnitude(),0.1);
        
    }
    @Test
    public void testDotProduct(){
        Vector3d vectorTest = new Vector3d(3,3,3);
        assertEquals(18,vectorTest.dotProduct(2,2,2));
    }
    @Test
    public void testAdd(){
        Vector3d vectorTest = new Vector3d(4,4,4);
        assertEquals("(6.0,6.0,6.0)", vectorTest.add(2,2,2));
    }

    
}
