package LinearAlgebra;

public class Vector3d {
    private double x;
    private double y;
    private double z;
public Vector3d(double x, double y,double z){
    this.x = x;
    this.y = y;
    this.z = z;
}
public double getX(){
    return this.x;
}
public double getY(){
    return this.y;
}
public double getZ(){
    return this.z;
}
public double magnitude(){
    return Math.sqrt(Math.pow(this.x,this.x)+ Math.pow(this.y,this.y) + Math.pow(this.z,this.z));
}
public double dotProduct(double x1, double y1, double z1){
    return ((x1*this.x)+(y1*this.y)+(z1*this.z));
}
public String add(double x1, double y1, double z1){
    return "("+(x1+this.x) +","+(y1+this.y)+","+(z1+this.z)+")";
}

}

